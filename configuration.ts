
// All of the game configurations are in this object and can be modified.
let gameConfig = {
    shipRotationDegreeUnit: 5,
    shipMoveUnit: 5,
    gameCycleInterval: 15,
    shootCoolDownCycles: 10,
    shootCoolDownRemaining: 0,
    bulletLifeTimeCycles: 300,
    bulletMoveUnit: 5,
    bulletRadius: 4,
    bulletColor: 'blue',
    asteroidMoveUnit: 2,
    asteroidRadius: 20,
    asteroidColor: 'purple',
    newAsteroidInterval: 250,
    maxAsteroidsAlive: 7,
    gameRunning: true,
    gamePaused: false
}

// Defines structure of a key accepted by the game.
interface gameKey {
    pressed: boolean,
    key: string
}

  /*
    All of the keys accepted by the game. Any key pressed not in this object will be ignored.
  */
  let gameKeys: { [name: string]: gameKey } = {
    left: {
      pressed: false,
      key: 'a'
    },
    right: {
      pressed: false,
      key: 'd'
    },
    up: {
      pressed: false,
      key: 'w'
    },
    down: {
      pressed: false,
      key: 's'
    },
    shoot: {
      pressed: false,
      key: ' '
    }
  }


  // Checks if a key is a valid key for this game.
  const isGameKey = (key: string) => key === gameKeys.left.key ||
                                     key === gameKeys.right.key ||
                                     key === gameKeys.up.key ||
                                     key === gameKeys.down.key ||
                                     key === gameKeys.shoot.key

                                    
  // Changes the pressed state of the given key.
  const changeKeyState = (key: string) => (newState: boolean) => { 
    if (key === gameKeys.left.key) gameKeys.left.pressed = newState
    if (key === gameKeys.right.key) gameKeys.right.pressed = newState
    if (key === gameKeys.up.key) gameKeys.up.pressed = newState
    if (key === gameKeys.down.key) gameKeys.down.pressed = newState
    if (key === gameKeys.shoot.key) gameKeys.shoot.pressed = newState
  }