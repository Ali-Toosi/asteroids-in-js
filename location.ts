  // Interface for location storing objects.
  interface MyLocation {
    x: number,
    y: number,
    dir: number,
  }

  // This function will check if the given value is outside of the grid for any dimension, it will put it back inside.
  const transformDim = (val: number) => (val + 600) % 600

  // Takes a new value for dir and makes sure it is between 0 and 359
  const transformDir = (val: number) => (val % 360)

  // Sets the position of given element to the given location on SVG
  const updateElementInDom = (element: Elem) => (location: MyLocation) => element.attr("transform", "translate(" + location.x.toString() + " " + location.y.toString() + ") rotate(" + (transformDir(180-location.dir)).toString() + ")")
  
  // This function takes a location object and changes its X attribute
  const updateLocationX = (location: MyLocation) => (newVal: number) => { location.x = transformDim(newVal) }

  // This funtion takes a location object and changes its Y attribute
  const updateLocationY = (location: MyLocation) => (newVal: number) => { location.y = transformDim(newVal) }

  // Takes a location, dimension name and a value to move that dimenstion by and updates the location object accordingly.
  const updateLocationByDim = (location: MyLocation) => (dimension: string) => (moveBy: number) => {
    if (dimension === 'x' || dimension === 'X')
      updateLocationX(location)(location.x + moveBy)
    else if (dimension === 'y' || dimension === 'Y')
      updateLocationY(location)(location.y + moveBy)
  }
  
  // Takes a vector (x, y) and moves to location with that vector
  const updateLocationByVector = (location: MyLocation) => (vector: {x: number, y: number}) => {
    updateLocationX(location)(location.x + vector.x)
    updateLocationY(location)(location.y + vector.y)
  }

  // Takes a direction (in degrees) and returns a vector of movement that should be done
  const calculateMoveVector = (moveUnit: number) => (dir: number) => (reverse: boolean) => {
    const dx = moveUnit * Math.sin((dir) * (Math.PI / 180))
    const dy = moveUnit * Math.cos((dir) * (Math.PI / 180))
    return { x: dx * (reverse ? -1 : 1), y: dy * (reverse ? -1 : 1)}
  }

  // Takes 2 locations and decides if they colide or not. Colission is defined as if they are closer than a given margin to each other.
  const colide = (l1: MyLocation) => (l2: MyLocation) => (margin: number) => Math.pow(l1.x - l2.x, 2) + Math.pow(l1.y - l2.y, 2) <= margin*margin