// FIT2102 2019 Assignment 1
// https://docs.google.com/document/d/1Gr-M6LTU-tfm4yabqZWJYg-zTjEVqHKKTCvePGCYsUA/edit?usp=sharing

/*
  Files modified and created for this project are:
    - asteroids.html
    - asteroids.ts
    - configuration.ts
    - location.ts

  Functionalities implemented in the game:
    - Ship movable by keyboard (w-a-s-d) and ship shoots with space.
    - Collision between asteroids and the ship ends the game.
    - Wrapping around the edges.
    - Ship can move in every direction.
    - If an asteroid hits the ship, a red circle will indicate it on the game and a "Game Over" sign will appear.
    - Big asteroids break down to smaller asteroids when destroy.
    - Game has multiple levels and gets harder when the levels go higher.
    - Game keeps track of score. Destroying each asteroid has one point.
    - Ship uses thrust movements.
    - Game can be pasued / resumed.
    - All of the variables such as bullet's radius, bullet's color, asteroid's radius and color, all of the moving speeds, etc. are
      in a single configuration object and can be modified.
    - Ship can move, rotate and shoot all at the same time.
    - Player can restart the game.
    - Bullets will disapear after some time moving in the map instead of going off the edges forever.


*/

function asteroids() {
  // Inside this function you will use the classes and functions 
  // defined in svgelement.ts and observable.ts
  // to add visuals to the svg element in asteroids.html, animate them, and make them interactive.
  // Study and complete the Observable tasks in the week 4 tutorial worksheet first to get ideas.

  // You will be marked on your functional programming style
  // as well as the functionality that you implement.
  // Document your code!  
  // Explain which ideas you have used ideas from the lectures to 
  // create reusable, generic functions.
  const svg = document.getElementById("canvas")!;
  // make a group for the spaceship and a transform to move it and rotate it
  // to animate the spaceship you will update the transform property
  let g = new Elem(svg,'g')
    .attr("transform","translate(300 300) rotate(0)")  
  
  // create a polygon shape for the space ship as a child of the transform group
  let ship = new Elem(svg, 'polygon', g.elem) 
    .attr("points","0,-20 15,20 -15,20")
    .attr("style","fill:lime;stroke:purple;stroke-width:1")

    
  // These variables are to store the player's score and level. When the level goes higher the game becomes harder.
  let score = 0, level = 1

  // This function will take an amount to increase the score by it and will then update the score label in game page.
  const increaseScore = ((amount: number) => {
    score += amount
    document.getElementById('score').innerHTML = score.toString()
  })

  // This function takes the game to the next level and makes it harder. Will also update the level label in the game page.
  const goToNextLevel = () => {
    level += 1
    document.getElementById('level').innerHTML = level.toString()
    gameConfig.asteroidMoveUnit += 1
    gameConfig.maxAsteroidsAlive += (level % 5 == 0 ? 1 : 0)
  }

  // This function will handle the pause attitude of the game. When user clicks on the button, if the game is not paused it will pause and if it's paused
  // it will resume. The pause behaviour is handled in the Observable and this function just changes the signal. It will also change the button label.
  document.getElementById("pauseButton").onclick = () => {
    if (gameConfig.gamePaused)
    {
      gameConfig.gamePaused = false
      document.getElementById('pauseButton').innerHTML = 'Pause Game'
    }
    else
    {
      gameConfig.gamePaused = true
      document.getElementById('pauseButton').innerHTML = 'Resume Game'
    }
  }
  document.getElementById("restartButton").onclick = () => { location.reload() }


  // Stores the ship position and moving direction. Moving direction is specified in degrees from 0 to 359.
  let shipLocation: MyLocation = {
    x: 300,
    y: 300,
    dir: 180
  }

  
  /*
    Describes attributes of a flying object
      element: The HTML element of the object in page.
      location: denotes the object's location in SVG. This is MyLocation object.
      reaminingLifeCycles: denotes how many cycles the object will be alive from now.
      radius: Since all of the objects are of a circle form, this denotes the radius for them.
      speed: Shows how many points will the object move at each cycle.
  */
  interface FlyingObject {
    element: Elem,
    location: MyLocation,
    remainingLifeCycles: number,
    radius: number,
    speed: number
  }

  // These arrays store the bullets and asteroids in the game
  let bullets: FlyingObject[] = []
  let asteroids: FlyingObject[] = []


  /*
    Takes ship location as a MyLocation object and based on the keys pressed currently, determines where should the ship move to.
  */
  const handleShipMovement = (location: MyLocation) => {
    let rotationDegree = 0
    rotationDegree += gameConfig.shipRotationDegreeUnit * (gameKeys.left.pressed ? 1 : 0)
    rotationDegree += -1 * gameConfig.shipRotationDegreeUnit * (gameKeys.right.pressed ? 1 : 0)
    location.dir = transformDir(rotationDegree + location.dir)

    let moveVector = calculateMoveVector(gameConfig.shipMoveUnit)(location.dir)(gameKeys.down.pressed && !gameKeys.up.pressed)
    if (gameKeys.up.pressed || gameKeys.down.pressed) updateLocationByVector(location)(moveVector)

    updateElementInDom(g)(location)
  }

  /*
    Takes ship location as a MyLocation object and if the player is pressing the space key, it will create bullets.
    Bullets can be created every X cycles where X is defined in the game config.
  */
  const handleShooting = (location: MyLocation) => {
    if (!gameKeys.shoot.pressed) return
    if (gameConfig.shootCoolDownRemaining > 0) {
      gameConfig.shootCoolDownRemaining -= 1
      return
    }
    let newBullet: FlyingObject = {
      element: new Elem(svg, 'g'),
      location: {
        x: location.x,
        y: location.y,
        dir: location.dir,
      },
      remainingLifeCycles: gameConfig.bulletLifeTimeCycles,
      radius: gameConfig.bulletRadius,
      speed: gameConfig.bulletMoveUnit
    }
    new Elem(svg, 'circle', newBullet.element.elem)
      .attr('r', gameConfig.bulletRadius)
      .attr('fill', gameConfig.bulletColor)
    bullets.push(newBullet)
    gameConfig.shootCoolDownRemaining = gameConfig.shootCoolDownCycles
  }

  /*
    This function takes a list of flying objects and updates their location in game based on their currect location, the direction they
    are moving on and their speed.
    It will also deduct their life cycles number if passed as true.
    If an object reaches its life it will be removed from the objects list and the new list will be returned as the result of the function.
  */
  const updateFlyingObjects = (deductLife: boolean) => (objects: FlyingObject[]) => {
    objects.forEach((object: FlyingObject) => {
      object.remainingLifeCycles -= deductLife ? 1 : 0
      updateLocationByVector(object.location)(calculateMoveVector(object.speed)(object.location.dir)(false))
      updateElementInDom(object.element)(object.location)
    })
    objects.filter((object: FlyingObject) => object.remainingLifeCycles <= 0).forEach((object: FlyingObject) => svg.removeChild(object.element.elem))
    return objects.filter((object: FlyingObject) => object.remainingLifeCycles > 0)
  }
  const updateBullets = updateFlyingObjects(true)
  const updateAsteroids = updateFlyingObjects(false)

  /*
    Creates a new asteroid object in the game.
    If a base asteroid is provided, it means this asteroid should be created as a result of base astroid being destroyed. If so, the 
    new asteroid will have smaller radius and higher speed and will be created at the same location as previous asteroid but in different direction.
    Otherwise, it will be created at a random location.
  */
  const newAsteroid = (baseAsteroid: FlyingObject|undefined = undefined) => {
    const rand = Math.random()
    let newAsteroid: FlyingObject = {
      element: new Elem(svg, 'g'),
      location: {
        x: baseAsteroid ? baseAsteroid.location.x : (rand > 0.5 ? Math.random() * 600 : 0),
        y: baseAsteroid ? baseAsteroid.location.y : (rand > 0.5 ? 0 : Math.random() * 600),
        dir: baseAsteroid ? baseAsteroid.location.dir + (Math.random() * 60 - 30) : Math.random() * 359
      },
      remainingLifeCycles: 1,
      radius: baseAsteroid ? baseAsteroid.radius/1.7 : gameConfig.asteroidRadius,
      speed: baseAsteroid ? baseAsteroid.speed + 1 : gameConfig.asteroidMoveUnit
    }
    new Elem(svg, 'circle', newAsteroid.element.elem)
      .attr('r', newAsteroid.radius)
      .attr('fill', gameConfig.asteroidColor)
    asteroids.push(newAsteroid)
  }

  /*
    Takes a list of all bullets in the game and for each asteroid checks if they colide, then remove the bullet and destroy the asteroid.
    If the asteroid radius is bigger than 15 it will break down to two smaller asteroids.
  */
  const breakAsteroids = (bullets: FlyingObject[]) => {
    asteroids
      .filter(asteroid => {
        let count: number = 0;
        bullets
          .filter((bullet: FlyingObject) => colide(asteroid.location)(bullet.location)(asteroid.radius + bullet.radius))
          .forEach((bullet: FlyingObject) => { bullet.remainingLifeCycles = 0; count += 1 })
        return count > 0
      })
      .forEach((asteroid: FlyingObject) => {
        asteroid.remainingLifeCycles = 0
        increaseScore(1)
        if (asteroid.radius > 15) {
          newAsteroid(asteroid)
          newAsteroid(asteroid)
        }
      })
  }

  /*
    Checks all of the astroids to see if they colide with the ship. If they do, it will indicate the coliding astroid in the game
    by drawing a red circle around it and the game will be over.
  */
  const checkGameOver = (asteroids: FlyingObject[]) => {
    let count = 0
    asteroids
      .filter((asteroid: FlyingObject) => colide(asteroid.location)(shipLocation)(asteroid.radius + 10))
      .forEach((asteroid: FlyingObject) => {
        count += 1
        new Elem(svg, 'circle')
          .attr('fill', 'transparent')
          .attr('stroke', 'red')
          .attr('stroke-width', 3)
          .attr('cx', asteroid.location.x)
          .attr('cy', asteroid.location.y)
          .attr('r', asteroid.radius + 5)
      })
    if (count > 0) {
      gameConfig.gameRunning = false
      document.getElementById('gameover').innerHTML = "Game Over!"
    }
  }

  /*
    Keeps track of buttons being pressed. If the button pressed is not a valid button for the game it will be filtered, otherwise,
    its state will be updates in the buttons state.
  */
  const keyDownObserver = Observable.fromEvent<KeyboardEvent>(document, "keydown")
  keyDownObserver
    .filter(({key}) => isGameKey(key))
    .subscribe(({key}) => {
      changeKeyState(key)(true)
    })

  /*
    Keeps track of buttons being released. If the button released is not a valid button for the game it will be filtered, otherwise,
    its state will be updates in the buttons state.
  */
  const keyUpObserver = Observable.fromEvent<KeyboardEvent>(document, "keyup")
  keyUpObserver
    .filter(({key}) => isGameKey(key))
    .subscribe(({key}) => {
      changeKeyState(key)(false)
    })

  /*
    This is the main game cycle obserable. It will run every X miliseconds (where X is defined in the gameConfig) and will run all of the above 
    functions to handle different functionalities of the game.

    This will also check for the game to be running and not paused. If paused, the observable will be filtered.
  */
  const mainCycle = Observable.interval(gameConfig.gameCycleInterval).filter(() => gameConfig.gameRunning && !gameConfig.gamePaused)
  mainCycle.subscribe(cycle => {
    handleShipMovement(shipLocation)
    handleShooting(shipLocation)
    bullets = updateBullets(bullets)
    asteroids = updateAsteroids(asteroids)
    breakAsteroids(bullets)
    checkGameOver(asteroids)
  })

  // This creates a new asteroid every X intervals if there are not maximum number of asteroids in the game, where X and the max number
  // of asteroids are deined in the game config.
  mainCycle.filter(cycle => cycle % gameConfig.newAsteroidInterval === 0)
    .subscribe(() => { if (asteroids.length < gameConfig.maxAsteroidsAlive) newAsteroid()})
  
  // This function takes the player to the next level every 2000 cycles.
  mainCycle.filter(cycle => cycle % 2000 === 0)
    .subscribe(() => goToNextLevel())
}

// the following simply runs your asteroids function on window load.  Make sure to leave it in place.
if (typeof window != 'undefined')
  window.onload = ()=>{
    asteroids();
  }

 

 
