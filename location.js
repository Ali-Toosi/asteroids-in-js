"use strict";
const transformDim = (val) => (val + 600) % 600;
const transformDir = (val) => (val % 360);
const updateElementInDom = (element) => (location) => element.attr("transform", "translate(" + location.x.toString() + " " + location.y.toString() + ") rotate(" + (transformDir(180 - location.dir)).toString() + ")");
const updateLocationX = (location) => (newVal) => { location.x = transformDim(newVal); };
const updateLocationY = (location) => (newVal) => { location.y = transformDim(newVal); };
const updateLocationByDim = (location) => (dimension) => (moveBy) => {
    if (dimension === 'x' || dimension === 'X')
        updateLocationX(location)(location.x + moveBy);
    else if (dimension === 'y' || dimension === 'Y')
        updateLocationY(location)(location.y + moveBy);
};
const updateLocationByVector = (location) => (vector) => {
    updateLocationX(location)(location.x + vector.x);
    updateLocationY(location)(location.y + vector.y);
};
const calculateMoveVector = (moveUnit) => (dir) => (reverse) => {
    const dx = moveUnit * Math.sin((dir) * (Math.PI / 180));
    const dy = moveUnit * Math.cos((dir) * (Math.PI / 180));
    return { x: dx * (reverse ? -1 : 1), y: dy * (reverse ? -1 : 1) };
};
const colide = (l1) => (l2) => (margin) => Math.pow(l1.x - l2.x, 2) + Math.pow(l1.y - l2.y, 2) <= margin * margin;
//# sourceMappingURL=location.js.map