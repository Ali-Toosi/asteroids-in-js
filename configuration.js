"use strict";
let gameConfig = {
    shipRotationDegreeUnit: 5,
    shipMoveUnit: 5,
    gameCycleInterval: 15,
    shootCoolDownCycles: 10,
    shootCoolDownRemaining: 0,
    bulletLifeTimeCycles: 300,
    bulletMoveUnit: 5,
    bulletRadius: 4,
    bulletColor: 'blue',
    asteroidMoveUnit: 2,
    asteroidRadius: 20,
    asteroidColor: 'purple',
    newAsteroidInterval: 250,
    maxAsteroidsAlive: 7,
    gameRunning: true,
    gamePaused: false
};
let gameKeys = {
    left: {
        pressed: false,
        key: 'a'
    },
    right: {
        pressed: false,
        key: 'd'
    },
    up: {
        pressed: false,
        key: 'w'
    },
    down: {
        pressed: false,
        key: 's'
    },
    shoot: {
        pressed: false,
        key: ' '
    }
};
const isGameKey = (key) => key === gameKeys.left.key ||
    key === gameKeys.right.key ||
    key === gameKeys.up.key ||
    key === gameKeys.down.key ||
    key === gameKeys.shoot.key;
const changeKeyState = (key) => (newState) => {
    if (key === gameKeys.left.key)
        gameKeys.left.pressed = newState;
    if (key === gameKeys.right.key)
        gameKeys.right.pressed = newState;
    if (key === gameKeys.up.key)
        gameKeys.up.pressed = newState;
    if (key === gameKeys.down.key)
        gameKeys.down.pressed = newState;
    if (key === gameKeys.shoot.key)
        gameKeys.shoot.pressed = newState;
};
//# sourceMappingURL=configuration.js.map