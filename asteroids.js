"use strict";
function asteroids() {
    const svg = document.getElementById("canvas");
    let g = new Elem(svg, 'g')
        .attr("transform", "translate(300 300) rotate(0)");
    let ship = new Elem(svg, 'polygon', g.elem)
        .attr("points", "0,-20 15,20 -15,20")
        .attr("style", "fill:lime;stroke:purple;stroke-width:1");
    let score = 0, level = 1;
    const increaseScore = ((amount) => {
        score += amount;
        document.getElementById('score').innerHTML = score.toString();
    });
    const goToNextLevel = () => {
        level += 1;
        document.getElementById('level').innerHTML = level.toString();
        gameConfig.asteroidMoveUnit += 1;
        gameConfig.maxAsteroidsAlive += (level % 5 == 0 ? 1 : 0);
    };
    document.getElementById("pauseButton").onclick = () => {
        if (gameConfig.gamePaused) {
            gameConfig.gamePaused = false;
            document.getElementById('pauseButton').innerHTML = 'Pause Game';
        }
        else {
            gameConfig.gamePaused = true;
            document.getElementById('pauseButton').innerHTML = 'Resume Game';
        }
    };
    document.getElementById("restartButton").onclick = () => { location.reload(); };
    let shipLocation = {
        x: 300,
        y: 300,
        dir: 180
    };
    let bullets = [];
    let asteroids = [];
    const handleShipMovement = (location) => {
        let rotationDegree = 0;
        rotationDegree += gameConfig.shipRotationDegreeUnit * (gameKeys.left.pressed ? 1 : 0);
        rotationDegree += -1 * gameConfig.shipRotationDegreeUnit * (gameKeys.right.pressed ? 1 : 0);
        location.dir = transformDir(rotationDegree + location.dir);
        let moveVector = calculateMoveVector(gameConfig.shipMoveUnit)(location.dir)(gameKeys.down.pressed && !gameKeys.up.pressed);
        if (gameKeys.up.pressed || gameKeys.down.pressed)
            updateLocationByVector(location)(moveVector);
        updateElementInDom(g)(location);
    };
    const handleShooting = (location) => {
        if (!gameKeys.shoot.pressed)
            return;
        if (gameConfig.shootCoolDownRemaining > 0) {
            gameConfig.shootCoolDownRemaining -= 1;
            return;
        }
        let newBullet = {
            element: new Elem(svg, 'g'),
            location: {
                x: location.x,
                y: location.y,
                dir: location.dir,
            },
            remainingLifeCycles: gameConfig.bulletLifeTimeCycles,
            radius: gameConfig.bulletRadius,
            speed: gameConfig.bulletMoveUnit
        };
        new Elem(svg, 'circle', newBullet.element.elem)
            .attr('r', gameConfig.bulletRadius)
            .attr('fill', gameConfig.bulletColor);
        bullets.push(newBullet);
        gameConfig.shootCoolDownRemaining = gameConfig.shootCoolDownCycles;
    };
    const updateFlyingObjects = (deductLife) => (objects) => {
        objects.forEach((object) => {
            object.remainingLifeCycles -= deductLife ? 1 : 0;
            updateLocationByVector(object.location)(calculateMoveVector(object.speed)(object.location.dir)(false));
            updateElementInDom(object.element)(object.location);
        });
        objects.filter((object) => object.remainingLifeCycles <= 0).forEach((object) => svg.removeChild(object.element.elem));
        return objects.filter((object) => object.remainingLifeCycles > 0);
    };
    const updateBullets = updateFlyingObjects(true);
    const updateAsteroids = updateFlyingObjects(false);
    const newAsteroid = (baseAsteroid = undefined) => {
        const rand = Math.random();
        let newAsteroid = {
            element: new Elem(svg, 'g'),
            location: {
                x: baseAsteroid ? baseAsteroid.location.x : (rand > 0.5 ? Math.random() * 600 : 0),
                y: baseAsteroid ? baseAsteroid.location.y : (rand > 0.5 ? 0 : Math.random() * 600),
                dir: baseAsteroid ? baseAsteroid.location.dir + (Math.random() * 60 - 30) : Math.random() * 359
            },
            remainingLifeCycles: 1,
            radius: baseAsteroid ? baseAsteroid.radius / 1.7 : gameConfig.asteroidRadius,
            speed: baseAsteroid ? baseAsteroid.speed + 1 : gameConfig.asteroidMoveUnit
        };
        new Elem(svg, 'circle', newAsteroid.element.elem)
            .attr('r', newAsteroid.radius)
            .attr('fill', gameConfig.asteroidColor);
        asteroids.push(newAsteroid);
    };
    const breakAsteroids = (bullets) => {
        asteroids
            .filter(asteroid => {
            let count = 0;
            bullets
                .filter((bullet) => colide(asteroid.location)(bullet.location)(asteroid.radius + bullet.radius))
                .forEach((bullet) => { bullet.remainingLifeCycles = 0; count += 1; });
            return count > 0;
        })
            .forEach((asteroid) => {
            asteroid.remainingLifeCycles = 0;
            increaseScore(1);
            if (asteroid.radius > 15) {
                newAsteroid(asteroid);
                newAsteroid(asteroid);
            }
        });
    };
    const checkGameOver = (asteroids) => {
        let count = 0;
        asteroids
            .filter((asteroid) => colide(asteroid.location)(shipLocation)(asteroid.radius + 10))
            .forEach((asteroid) => {
            count += 1;
            new Elem(svg, 'circle')
                .attr('fill', 'transparent')
                .attr('stroke', 'red')
                .attr('stroke-width', 3)
                .attr('cx', asteroid.location.x)
                .attr('cy', asteroid.location.y)
                .attr('r', asteroid.radius + 5);
        });
        if (count > 0) {
            gameConfig.gameRunning = false;
            document.getElementById('gameover').innerHTML = "Game Over!";
        }
    };
    const keyDownObserver = Observable.fromEvent(document, "keydown");
    keyDownObserver
        .filter(({ key }) => isGameKey(key))
        .subscribe(({ key }) => {
        changeKeyState(key)(true);
    });
    const keyUpObserver = Observable.fromEvent(document, "keyup");
    keyUpObserver
        .filter(({ key }) => isGameKey(key))
        .subscribe(({ key }) => {
        changeKeyState(key)(false);
    });
    const mainCycle = Observable.interval(gameConfig.gameCycleInterval).filter(() => gameConfig.gameRunning && !gameConfig.gamePaused);
    mainCycle.subscribe(cycle => {
        handleShipMovement(shipLocation);
        handleShooting(shipLocation);
        bullets = updateBullets(bullets);
        asteroids = updateAsteroids(asteroids);
        breakAsteroids(bullets);
        checkGameOver(asteroids);
    });
    mainCycle.filter(cycle => cycle % gameConfig.newAsteroidInterval === 0)
        .subscribe(() => { if (asteroids.length < gameConfig.maxAsteroidsAlive)
        newAsteroid(); });
    mainCycle.filter(cycle => cycle % 2000 === 0)
        .subscribe(() => goToNextLevel());
}
if (typeof window != 'undefined')
    window.onload = () => {
        asteroids();
    };
//# sourceMappingURL=asteroids.js.map